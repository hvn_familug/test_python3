sinput = """2
2
000000000000000000000000000004
000000000000000001111000000004
000000000000000001111020000004
000000000000000000111020000004
000000000000000001111220000004
000000000000000001111000000004
000000000000000000000000000004
000000000000000000000000000004
000000000000000000000000000004
000000000000000000000000000004
000000000000000000000000000004
000000000000000000000000000004
000000000000000000000000000004
000000000000000000000000000004
000000000000000000000000004444
000000000000000000000000004000
000000000000000000000033304000
000000000000000000000033344000
000000000000000000000033300000
000000000000000000000000000000
2 22
0 29
"""

MOVES = ["RIGHT", "DOWN", "LEFT", "UP"]
POS_CHANGE = {"RIGHT": (0, 1), "LEFT": (0, -1), "UP": (-1, 0), "DOWN": (1, 0)}

def invalid_moves(board, last_move, position, my_id):
    invalid_moves = []
    for move in MOVES:
        new_position = next_position(position, move)
        if check_wall_limit(new_position) or check_unstable(board, new_position, my_id) or check_invalid_backward(board, last_move, move, position, my_id):
            invalid_moves.append(move)
    return invalid_moves

def check_valid(board, position, last_move, move, my_id):
    return move not in invalid_moves(board, last_move, position, my_id)

def check_wall_limit(position):
    return (position[0] > 19) or (position[1] > 29) or (position[0] < 0) or (position[1] < 0)

def check_unstable(board, position, my_id):
    return board[position[0]][position[1]] == (my_id * 2)

def check_invalid_backward(board, last_move, move, position, my_id):
    if last_move == "":
        return False
    if check_unstable(board, position, my_id):
        return MOVES[(MOVES.index(last_move) + 2) % len(MOVES)] == move
    return False

def next_position(position, move):
    new_position = position[::]
    new_position[0] += POS_CHANGE[move][0]
    new_position[1] += POS_CHANGE[move][1]
    return new_position

def readboard():
    board = []
    for i in range(20):
        board.append(map(int, list(raw_input().strip())))
    return board


def readplayers(n_players):
    p_coors = {}
    for pid in range(1, n_players + 1):
        p_coors[pid] = (map(int, raw_input().strip().split()))
    return p_coors

def get_op_id(n_players, my_id):
    for i in range(1, n_players + 1):
        if i != my_id:
            return i

def get_op_move(last_pos, current_pos):
    if current_pos[0] - last_pos[0] == 1:
        return "DOWN"
    elif current_pos[0] - last_pos[0] == -1:
        return "UP"
    elif current_pos[1] - last_pos[1] == -1:
        return "LEFT"
    elif current_pos[1] - last_pos[1] == 1:
        return "RIGHT"

def move_to_near_op_unstable(board, position, op_id):
    op_unstable_id = op_id * 2
    for move in MOVES:
        checking_position = position[::]
        checking_position[0] += POS_CHANGE[move][0]
        checking_position[1] += POS_CHANGE[move][1]
        if checking_position[0] < 0 or checking_position[1] < 0 or checking_position[0] > 19 or checking_position[1] > 29:
            continue
        if board[checking_position[0]][checking_position[1]] == op_unstable_id:
            return (True, move)
    return (False, "")

def main():
    import sys, StringIO # NOQA
    # sys.stdin = StringIO.StringIO(sinput)
    n_players = int(raw_input().strip())
    my_id = int(raw_input().strip())
    # instructions = 'right right right down down down left left left up up up'.split()  # NOQA
    # moves = [i.upper() for i in instructions]

    cntr = 0
    repeat_move = 2
    readboard_flag = True
    copy_flag = False
    op_id = get_op_id(n_players, my_id)
    last_op_pos = [0,0]
    last_move = ""
    while True:
        board = readboard()  # each row 1 line
        players = readplayers(n_players)
        position = players[my_id]
        current_op_pos = players[op_id]
        kill_flag, move = move_to_near_op_unstable(board, position, op_id)
        if kill_flag:
            print move
            last_move = move
        elif not copy_flag:
            for move in MOVES:
                if check_valid(board, position, last_move, move, my_id):
                    print move
                    last_move = move
                    copy_flag = True
                    break
        else:
            op_move = get_op_move(current_op_pos, last_op_pos)
            if check_valid(board, position, last_move, op_move, my_id):
                last_move = op_move
                print op_move
            else:
                for move in MOVES:
                    if check_valid(board, position, last_move, move, my_id):
                        print move
                        last_move = move
                        break
        last_op_pos = current_op_pos


if __name__ == "__main__":
    main()
