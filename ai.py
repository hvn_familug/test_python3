sinput = """2
2
000000000000000000000000000004
000000000000000001111000000004
000000000000000001111020000004
000000000000000000111020000004
000000000000000001111220000004
000000000000000001111000000004
000000000000000000000000000004
000000000000000000000000000004
000000000000000000000000000004
000000000000000000000000000004
000000000000000000000000000004
000000000000000000000000000004
000000000000000000000000000004
000000000000000000000000000004
000000000000000000000000004444
000000000000000000000000004000
000000000000000000000033304000
000000000000000000000033344000
000000000000000000000033300000
000000000000000000000000000000
2 22
0 29
"""

MOVES = ["RIGHT", "DOWN", "LEFT", "UP"]
POS_CHANGE = {"RIGHT": (0, 1), "LEFT": (0, -1), "UP": (-1, 0), "DOWN": (1, 0)}

def invalid_moves(board, position, my_id):
    invalid_moves = []
    for move in MOVES:
        new_position = next_position(position, move)
        if check_wall_limit(new_position) or check_unstable(board, new_position, my_id):
            invalid_moves.append(move)
    return invalid_moves

def check_valid(board, position, move, my_id):
    return move not in invalid_moves(board, position, my_id)

def check_wall_limit(position):
    return (position[0] > 19) or (position[1] > 29) or (position[0] < 0) or (position[1] < 0)

def check_unstable(board, position, my_id):
    return board[position[0]][position[1]] == (my_id * 2)

def next_position(position, move):
    new_position = position[::]
    new_position[0] += POS_CHANGE[move][0]
    new_position[1] += POS_CHANGE[move][1]
    return new_position

def readboard():
    board = []
    for i in range(20):
        board.append(map(int, list(raw_input().strip())))
    return board


def readplayers(n_players):
    p_coors = {}
    for pid in range(1, n_players + 1):
        p_coors[pid] = (map(int, raw_input().strip().split()))
    return p_coors


def main():
    import sys, StringIO # NOQA
    # sys.stdin = StringIO.StringIO(sinput)
    n_players = int(raw_input().strip())
    my_id = int(raw_input().strip())
    # instructions = 'right right right down down down left left left up up up'.split()  # NOQA
    # moves = [i.upper() for i in instructions]

    cntr = 0
    repeat_move = 2
    readboard_flag = True
    while True:
        for move in MOVES:
            # print(n_players, my_id, board, players)
            for turn in range(repeat_move):
                if readboard_flag:
                    board = readboard()  # each row 1 line
                    players = readplayers(n_players)
                    position = players[my_id]
                if check_valid(board, position, move, my_id):
                    readboard_flag = True
                    print move
                else:
                    readboard_flag = False
                    break
        repeat_move += 1


if __name__ == "__main__":
    main()
